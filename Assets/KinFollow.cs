﻿using UnityEngine;
using System.Collections;

public class KinFollow : MonoBehaviour 
{
	
//	Camera main_cam;
	Ray ray;
	RaycastHit hit;
	CharacterController _controller;
	float rot_speed = 5f;
//	float gravity = 1000f;
	float speed = 200f;
	Vector3 velocity;
	Quaternion rotation;
	float distance;
	
	// Use this for initialization
	void Start () 
	{
	//	main_cam = Camera.main;
		_controller = this.GetComponent<CharacterController>();
		velocity = Vector3.zero;
	}

	// Update is called once per frame
	void Update () 
	{
			if( Input.GetMouseButtonDown(0) )
			{
				ray = Camera.main.ScreenPointToRay( Input.mousePosition );
				
				Debug.Log ( ray.ToString() );
				
				if (!Physics.Raycast( ray, out hit))
				{
					return;
				}
			}
		velocity = hit.point - this.transform.position;
		distance = velocity.magnitude;
		rotation.SetLookRotation(velocity);
		//velocity = transform.TransformDirection(velocity);
		//velocity *= speed;


		if ( distance > 10 ) 
		{
			_controller.transform.rotation = Quaternion.Slerp(this.transform.rotation, rotation, rot_speed * Time.deltaTime);
			transform.Translate(Vector3.forward * speed * Time.deltaTime);
			//_controller.Move( velocity.normalized * speed * Time.deltaTime);
		}
	}
}